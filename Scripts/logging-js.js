﻿// Инициализация серверного логирования Javascript-ошибок
function initializeServerLoggingJS() {
    if (document.getElementById("enableJSLogging").value == "1") {
        supportXMLHttpRequestInIE();
        window.onerror = customErrorHandler;
    }
}

// Поддержка XMLHttpRequest в Internet Explorer
function supportXMLHttpRequestInIE() {
    var xmlVersions = new Array(
        "Msxml2.XMLHTTP.6.0",
        "MSXML2.XMLHTTP.3.0",
        "MSXML2.XMLHTTP",
        "Microsoft.XMLHTTP"
    );
    if (typeof XMLHttpRequest == "undefined") XMLHttpRequest = function () {
        for (var i in xmlVersions) {
            try { return new ActiveXObject(xmlVersions[i]); }
            catch (e) {}
        }
        throw new Error("This browser does not support XMLHttpRequest.");
    };
}

// Новый обработчик ошибок для поддержки серверного логирования
function customErrorHandler(message, url, line) {
    var serverLoggingUrl = location.protocol + "//" + window.location.toString().split("/")[2] + '/project/Logging/LoggingJS.aspx';
    var params = "message=" + message + "&url=" + url + "&line=" + line;
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('POST', serverLoggingUrl, true);
    httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    httpRequest.send(params);
    return true;
}

// Генератор 
function wordsGenerator(wordsCount) {
    var s = '';
    var alphabet = 'abcdefghijklmnopqrstuvwxyz0123456789';
    var alphabetL = alphabet.length;
    while (s.length < wordsCount)
        s += alphabet[Math.random() * alphabetL | 0];
    return s;
}
function generateFakeJSError() {
    throw new Error(wordsGenerator(6));
}