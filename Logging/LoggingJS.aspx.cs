﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Logging_LoggingJS : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {        
        switch (Request.RequestType)
        {
            case "POST":
                if (GlobalVariables.enableJSLogging == 1)
                {
                    String serverRemoteAddr = Request.ServerVariables["REMOTE_ADDR"];

                    // Проверить частоту запросов с конкретных IP-адресов. Предотвратить DDOS-атаку
                    Int16 availableMessageCount = 1000;
                    Int16 availableMessagePeriod = 1;
                    bool isDDOSAttack = false;
                    if (Session[serverRemoteAddr] == null)
                        Session.Add(serverRemoteAddr, String.Format("{0}|{1}", DateTime.Now, 1));
                    else
                    {
                        String[] sessionRecord = Session[serverRemoteAddr].ToString().Split('|');
                        DateTime n = DateTime.Parse(sessionRecord[0]);
                        if ((DateTime.Now - DateTime.Parse(sessionRecord[0])).Seconds < availableMessagePeriod)
                        {
                            Int16 currentRecordCount = Int16.Parse(sessionRecord[1]);
                            if (currentRecordCount < availableMessageCount)
                                Session[serverRemoteAddr] = String.Format("{0}|{1}", sessionRecord[0], currentRecordCount);
                            else
                                isDDOSAttack = true;
                        }
                        else
                        {
                            Session[serverRemoteAddr] = String.Format("{0}|{1}", DateTime.Now, 1);
                        }
                    }

                    if (!isDDOSAttack)
                    {
                        using (StreamWriter jsErrorLog = new StreamWriter(Server.MapPath("~/Log/js-error.log"), true))
                        {
                            jsErrorLog.WriteLine(String.Format("[{0}] : Line {1} - {2} - {3}. Browser: {4}. Page: {5}. IP-address: {6} ",
                                DateTime.Now,
                                Request.Form["line"],
                                Request.Form["message"],
                                Request.Form["url"],
                                Request.ServerVariables["HTTP_USER_AGENT"],
                                Request.ServerVariables["HTTP_REFERER"],
                                serverRemoteAddr
                            ));
                        }
                    }
                }
                break;

            default:
                Response.StatusCode = 404;
                Server.Transfer("~/ErrorPages/404.aspx");
                break;
        }   
    }
}
